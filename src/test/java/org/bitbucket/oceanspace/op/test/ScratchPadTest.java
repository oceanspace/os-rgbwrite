/**
 * 
 */
package org.bitbucket.oceanspace.op.test;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.esa.beam.framework.dataio.ProductIO;
import org.esa.beam.framework.datamodel.Band;
import org.esa.beam.framework.datamodel.Product;
import org.esa.beam.framework.gpf.GPF;
import org.esa.beam.framework.gpf.OperatorSpi;
import org.esa.beam.framework.gpf.graph.Graph;
import org.esa.beam.framework.gpf.graph.GraphIO;
import org.esa.beam.framework.gpf.graph.GraphProcessor;
import org.esa.beam.gpf.operators.standard.ReadOp;
import org.esa.beam.gpf.operators.standard.SubsetOp;
import org.esa.beam.gpf.operators.standard.WriteOp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.bc.ceres.core.ProgressMonitor;

/**
 * Place to try out various test approaches. Any tests of concrete functionality should not be kept
 * here.
 * 
 * This test is ignored except when introducing some new feature.
 * 
 * @author kutila
 */
public class ScratchPadTest
{
    
    private File outputFile;
    
    private ReadOp.Spi readSpi;
    private OperatorSpi writeSpi;
    private SubsetOp.Spi subsetSpi;
    
    @Before
    public void setUp() throws Exception
    {
        this.readSpi = new ReadOp.Spi();
        this.writeSpi = new WriteOp.Spi();
        this.subsetSpi = new SubsetOp.Spi();
        GPF.getDefaultInstance().getOperatorSpiRegistry().addOperatorSpi(this.readSpi);
        GPF.getDefaultInstance().getOperatorSpiRegistry().addOperatorSpi(this.writeSpi);
        GPF.getDefaultInstance().getOperatorSpiRegistry().addOperatorSpi(this.subsetSpi);
    }
    
    @After
    public void tearDown() throws Exception
    {
        GPF.getDefaultInstance().getOperatorSpiRegistry().removeOperatorSpi(this.writeSpi);
        GPF.getDefaultInstance().getOperatorSpiRegistry().removeOperatorSpi(this.readSpi);
        GPF.getDefaultInstance().getOperatorSpiRegistry().removeOperatorSpi(this.subsetSpi);
    }
    
    /**
     * Use GraphProcessor to execute a graph with a simple read-write pipeline
     */
    @Ignore
    @Test
    public void testGraphProcessorReadWrite() throws Exception
    {
        
        // prepare: read Graph XML
        final InputStream resourceAsStream = this.getClass().getResourceAsStream(TestConstants.TEST_GRAPH_READ_WRITE);
        Assert.assertNotNull("resource was NULL", resourceAsStream);
        
        final Graph graph = GraphIO.read(new InputStreamReader(resourceAsStream));
        
        // do it: execute graph
        final GraphProcessor processor = new GraphProcessor();
        processor.executeGraph(graph, ProgressMonitor.NULL);
        
        // XXX: this is dependent on system file structure and WILL fail!
        this.outputFile = new File("/home/kutila/workspace/os-gpf/os-rgbwrite/target/read-write-out.N1");
        
        // verify:
        final Product productOnDisk = ProductIO.readProduct(this.outputFile);
        Assert.assertNotNull("Output was null", productOnDisk);
        
        Assert.assertEquals("read-write-out", productOnDisk.getName());
        Assert.assertEquals("Incorrect no. of bands", 4, productOnDisk.getNumBands());
        Assert.assertEquals("Incorrect band name", "band1", productOnDisk.getBandAt(0).getName());
        Assert.assertEquals("Incorrect band name", "band2", productOnDisk.getBandAt(1).getName());
        Assert.assertEquals("Incorrect band name", "band3", productOnDisk.getBandAt(2).getName());
        Assert.assertEquals("Incorrect band name", "band4", productOnDisk.getBandAt(3).getName());
        
        final Band band2 = productOnDisk.getBand("band2");
        band2.loadRasterData();
        Assert.assertTrue("Invalid Pixel Value", (2.5f - band2.getPixelFloat(0, 0)) < 0.0001);
        
        productOnDisk.dispose();
        this.outputFile.deleteOnExit();
    }
    
    /**
     * Simple test to validate that template writing with Velocity works
     */
    @Ignore
    @Test
    public void testVelocityTemplateWriting() throws Exception
    {
        final String valueInputFile = "/some/path/input-file.N1";
        final String valueOutputFile = "/output/dir/output-file.HDF";
        
        final InputStream resourceAsStream = this.getClass().getResourceAsStream("/graphs/read-write.vm.xml");
        Assert.assertNotNull("Could not load velocity template", resourceAsStream);
        
        final VelocityContext context = new VelocityContext();
        context.put("infile", valueInputFile);
        context.put("outfile", valueOutputFile);
        // context.put("outformat", "NetCDF-BEAM");
        
        final StringWriter writer = new StringWriter();
        Velocity.evaluate(context, writer, "", new InputStreamReader(resourceAsStream));
        
        final String outputString = writer.toString();
        
        Assert.assertTrue("Template not processed correctly",
                outputString.contains("<file>" + valueInputFile + "</file>"));
        Assert.assertTrue("Template not processed correctly", outputString.contains(valueOutputFile));
        Assert.assertTrue("not-replace template value missing", outputString.contains("${outformat}"));
        
    }
    
}
