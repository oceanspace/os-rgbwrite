package org.bitbucket.oceanspace.op.test;

public interface TestConstants
{
    
    public static final String TEST_GRAPH_READ_WRITE = "/graphs/read-write.xml";
    
    public static final String TEST_GRAPH_WRITE_3BANDS_TO_PNG = "/graphs/write-3bands-to-png.vm.xml";
    
    public static final String TEST_GRAPH_WRITE_1BAND_TO_PNG = "/graphs/write-1band-to-png.vm.xml";
 
    
    public static final String TEST_PRODUCT_AATSR_TOA_N1 = "/dataproducts/ATS_TOA_1CNPDK20030504_111259_000000572016_00080_06146_0157.N1";

    public static final String TEST_PRODUCT_SGBR_AVG_N1 = "/dataproducts/sgbr-avg.N1";

    
    public static final String TEST_COLOR_PALETTE_FILE_CHLOR = "/lookups/chlor1.cpd";
    
}