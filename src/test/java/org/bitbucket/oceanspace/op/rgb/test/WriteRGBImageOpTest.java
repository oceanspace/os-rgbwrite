package org.bitbucket.oceanspace.op.rgb.test;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.bitbucket.oceanspace.op.test.TestConstants;
import org.esa.beam.framework.dataio.ProductIO;
import org.esa.beam.framework.datamodel.Product;
import org.esa.beam.framework.gpf.GPF;
import org.esa.beam.framework.gpf.graph.Graph;
import org.esa.beam.framework.gpf.graph.GraphIO;
import org.esa.beam.framework.gpf.graph.GraphProcessor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bc.ceres.core.ProgressMonitor;

public class WriteRGBImageOpTest
{
    
    private File outputFile;
    
    @Before
    public void setUp() throws Exception
    {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
    }
    
    @After
    public void tearDown() throws Exception
    {
        if(outputFile != null && outputFile.exists())
        {
            outputFile.deleteOnExit();
        }
    }
    
    /**
     * Use GraphProcessor to read a data product and generate a PNG image from one of its bands.
     */
    @Test
    public void testWriteRGBImage() throws Exception
    {
        final URL testResourceUrl = this.getClass().getResource(TestConstants.TEST_PRODUCT_SGBR_AVG_N1);
        final URL testColorPaletteDefUrl = this.getClass().getResource(TestConstants.TEST_COLOR_PALETTE_FILE_CHLOR);
        
        final String sourceBandName = "AVG_BAND";
        final String testRGBOutputPath = "target/rgb-out.png";
        final String format = "PNG";
        final String testOutputProductPath = "target/rgb-out.dim";
        
        // prepare: build Graph XML from velocity template
        final InputStream resourceAsStream =
                this.getClass().getResourceAsStream(TestConstants.TEST_GRAPH_WRITE_1BAND_TO_PNG);
        Assert.assertNotNull("Could not load Graph file template", resourceAsStream);
        
        final VelocityContext context = new VelocityContext();
        context.put("infile", testResourceUrl.getFile());
        context.put("sourceBandName", sourceBandName);
        context.put("cpdFilePath", testColorPaletteDefUrl.getFile());
        context.put("formatName", format);
        context.put("rgbOutputPath", testRGBOutputPath);
        context.put("outProductPath", testOutputProductPath);

        context.put("scaleType", "lin");
        context.put("colourScaleMin", "0.0");
        context.put("colourScaleMax", "10.00");
        context.put("scalingValue", "9.25");
        context.put("missingData", "-1.0");
        
        final StringWriter writer = new StringWriter();
        Velocity.evaluate(context, writer, "", new InputStreamReader(resourceAsStream));
        
        final Graph graph = GraphIO.read(new StringReader(writer.toString()));
        
        // do it: execute graph
        final GraphProcessor processor = new GraphProcessor();
        processor.executeGraph(graph, ProgressMonitor.NULL);
        
        // verify:
        outputFile = new File(testRGBOutputPath);
        Assert.assertTrue("Image file not created", outputFile.exists());
        
        final Product productOnDisk = ProductIO.readProduct(outputFile);
        Assert.assertNotNull("Output image was null", productOnDisk);
        final String[] bandNames = productOnDisk.getBandNames();
        Assert.assertEquals("Image should have 5 bands", 5, bandNames.length);
        for(int i = 0; i < bandNames.length; i++)
        {
            System.out.println(bandNames[i]);
        }
        
        productOnDisk.dispose();
    }
    
}
