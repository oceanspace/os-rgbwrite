package org.bitbucket.oceanspace.op.rgb.test;

import java.awt.image.RenderedImage;

import javax.media.jai.JAI;

import org.bitbucket.oceanspace.op.rgb.RGBUtils;
import org.esa.beam.framework.dataio.ProductIO;
import org.esa.beam.framework.datamodel.Band;
import org.esa.beam.framework.datamodel.ColorPaletteDef;
import org.esa.beam.framework.datamodel.ImageInfo;
import org.esa.beam.framework.datamodel.Product;
import org.esa.beam.framework.datamodel.RasterDataNode;
import org.esa.beam.framework.datamodel.Stx;
import org.esa.beam.jai.ImageManager;
import org.junit.Assert;

import com.bc.ceres.core.ProgressMonitor;

/**
 * Simple test to write an RGB image from a single band using a color palette.
 */
public class SimpleRgb
{
    // parameter
    
    // color table
    private String clutPath = "/home/kutila/workspace/os-rgbwrite/src/test/resources/chlor.lut";
    
    // source product
    private String sourcePath = "/home/kutila/workspace/os-rgbwrite/src/test/resources/dataproducts/" +
    		//"sgbr-avg.N1";
      "marsi-20131021/M-L3-GBR-5D-1D-CHLOR-D-SGBR-5D-2013251-2013255_L3-L3_HDF2PNG-100_outlier_scaled.hdf";
    
    // source band
    private String sourceBandName = 
            //"AVG_BAND";
            "chl_scaled";
    
    // target output
    private String targetPath = "/home/kutila/workspace/os-rgbwrite/target/";
    private String formatName = "png";
    
    private int level = 0; // increase for smaller images
    
    // no data value
    private double noData = -1.0;
    
    // range
    private double min = 0.01;
    private double max = 1.0;
    
    private double[] histoSkipRatios = {0.01, 0.04};
    
    // scaling - none, linear, logarithmic
    
    /**
     * @param args
     */
    public static void main(String[] args) throws Exception
    {
        System.out.println("=====================");
        new SimpleRgb().test();
        System.out.println("=====================");
    }
    
    public void test() throws Exception
    {
        // read source Product
        final Product sourceProduct = ProductIO.readProduct(sourcePath);
        Assert.assertNotNull(sourceProduct);
        Assert.assertNotNull(sourceProduct.getBand(sourceBandName));

        ColorPaletteDef cpd = RGBUtils.buildColorPaletteDefFromFile(min, max, noData, clutPath);
        
        // prepare color Band
        RasterDataNode[] bands = new Band[1];
        Band band = sourceProduct.getBand(sourceBandName);
        final ImageInfo defaultImageInfo = band.createDefaultImageInfo(histoSkipRatios, ProgressMonitor.NULL);
        band.setImageInfo(defaultImageInfo);
        
        if (band.getIndexCoding() != null) {
            band.getImageInfo().setColors(cpd.getColors());
        } else {
            Stx stx = band.getStx();
            band.getImageInfo().setColorPaletteDef(cpd,
                                                   stx.getMinimum(),
                                                   stx.getMaximum(), false);
        }
        
        bands[0] = band;
        
        
        //defaultImageInfo.setHistogramMatching(ImageInfo.getHistogramMatching("normalize"));
        //ImageInfo defaultImageInfo = new ImageInfo(cpd);
        
        // construct Image
        ImageManager imageManager = ImageManager.getInstance();
        RenderedImage outputImage = imageManager.createColoredBandImage(bands, defaultImageInfo, level);
        
        // write Image
        String outputFilePath = targetPath + System.currentTimeMillis() + "." + formatName;
        JAI.create("filestore", outputImage, outputFilePath, formatName, null);
    }
    
}
